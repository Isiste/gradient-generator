<?php

namespace cubegroup\gradient;



class GradientAsset extends \cubegroup\base\AssetBundle{
  public $jsOptions = ['position' => \yii\web\View::POS_END]; 
  public function init()
    {
        $this->setSourcePath(__DIR__ . '/');
        $this->setupAssets('css', ['css/main']);
        $this->setupAssets('js', ['js/main']);
        
        parent::init();
    }
    
}
