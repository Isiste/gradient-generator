<?php

use kartik\slider\Slider;

;?>

<div id="gradientGen" class="row">
    <div class="col-md-6">
        <ul id="gradientOptions">
            <li><label for="gradientType">Typ</label> <select name="gradientType" id="gradientType" class="form-control input-sm"><option value="linear">Liniowy</option><option value="radial">Promieniowy</option></select></li>
            <!--  kolor start  -->
            
            <li class="poczatek"><p class="title">Wartości początkowe</p></li>
            <li class="poczatek">
                <label for="redStart">Czerwony</label>
                <?=  Slider::widget([
                    'name'=>'redStart',
                    'id'=>'redStart',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_DANGER,
                    'handleColor'=>Slider::TYPE_DANGER,
                    'pluginOptions'=>[
                        'handle'=>'vertical',
                        'min'=>0,
                        'max'=>255,
                        'step'=>0.00001
                    ]
                ]);
            ;?>
            </li>
            <li class="poczatek">
                <label for="greenStart">Zielony</label>
                <?=  Slider::widget([
                    'name'=>'greenStart',
                    'id'=>'greenStart',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_SUCCESS,
                    'handleColor'=>Slider::TYPE_SUCCESS,
                    'pluginOptions'=>[
                        'handle'=>'vertical',
                        'min'=>0,
                        'max'=>255,
                        'step'=>0.00001
                    ]
                ]);
            ;?>
            </li>
            <li class="poczatek">
                <label for="blueStart">Niebieski</label>
                <?=  Slider::widget([
                    'name'=>'blueStart',
                    'id'=>'blueStart',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_PRIMARY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'vertical',
                        'min'=>0,
                        'max'=>255,
                        'step'=>0.00001
                    ]
                ]);
            ;?>
            </li>
            <li class="poczatek">
                <label for="hPosStart">Pozycja h</label>
                 <?=  Slider::widget([
                    'name'=>'hPosStart',
                    'id'=>'hPosStart',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_GREY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'triangle',
                        'min'=>-1000,
                        'max'=>1000,
                        'step'=>0.00001
                    ]
                ]);?>
            </li>
             <li class="poczatek">
                 <label for="vPosStart">Pozycja v</label>
                 <?=  Slider::widget([
                    'name'=>'vPosStart',
                    'id'=>'vPosStart',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_GREY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'square',
                        'min'=>-1000,
                        'max'=>1000,
                        'step'=>0.00001
                    ]
                ]);?>
            </li>
            <li class="radial poczatek" >
                <label for="radialStart">Promień</label>
                 <?=  Slider::widget([
                    'name'=>'radialStart',
                    'id'=>'radialStart',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_GREY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'square',
                        'min'=>-1000,
                        'max'=>1000,
                        'step'=>0.00001
                    ]
                ]);?>
            </li>
            <!-- koniec -->
            <li class="koniec"><p class="title">Wartości końcowe</p></li>
            <li class="koniec">
                <label for="redKoniec">Czerwony</label>
                <?=  Slider::widget([
                    'name'=>'redKoniec',
                    'id'=>'redKoniec',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_DANGER,
                    'handleColor'=>Slider::TYPE_DANGER,
                    'pluginOptions'=>[
                        'handle'=>'vertical',
                        'min'=>0,
                        'max'=>255,
                        'step'=>0.00001
                    ]
                ]);
            ;?>
            </li>
            <li  class="koniec">
                <label for="greenKoniec">Zielony</label>
                <?=  Slider::widget([
                    'name'=>'greenKoniec',
                    'id'=>'greenKoniec',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_SUCCESS,
                    'handleColor'=>Slider::TYPE_SUCCESS,
                    'pluginOptions'=>[
                        'handle'=>'vertical',
                        'min'=>0,
                        'max'=>255,
                        'step'=>0.00001
                    ]
                ]);
            ;?>
            </li>
            <li class="koniec">
                <label for="blueKoniec">Niebieski</label>
                <?=  Slider::widget([
                    'name'=>'blueKoniec',
                    'id'=>'blueKoniec',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_PRIMARY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'vertical',
                        'min'=>0,
                        'max'=>255,
                        'step'=>0.00001
                    ]
                ]);
            ;?>
            </li>
            <li class="koniec">
                 <label for="hPosKoniec">Pozycja h</label>
                 <?=  Slider::widget([
                    'name'=>'hPosKoniec',
                    'id'=>'hPosKoniec',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_GREY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'triangle',
                        'min'=>-1000,
                        'max'=>1000,
                        'step'=>0.00001
                    ]
                ]);?>
            </li>
             <li class="koniec">
                 <label for="vPosKoniec">Pozycja v</label>
                 <?=  Slider::widget([
                    'name'=>'vPosKoniec',
                    'id'=>'vPosKoniec',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_GREY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'square',
                        'min'=>-1000,
                        'max'=>1000,
                        'step'=>0.00001
                    ]
                ]);?>
            </li>
            <li class="radial koniec" >
                <label for="radialKoniec">Promień</label>
                 <?=  Slider::widget([
                    'name'=>'radialKoniec',
                    'id'=>'radialKoniec',
                    'value'=>0,
                    'sliderColor'=>Slider::TYPE_GREY,
                    'handleColor'=>Slider::TYPE_PRIMARY,
                    'pluginOptions'=>[
                        'handle'=>'square',
                        'min'=>-1000,
                        'max'=>1000,
                        'step'=>0.00001
                    ]
                ]);?>
            </li>
        </ul> 
    </div>
    <div class="col-md-6">
        <div id="wypelnienieGradient">
            
        </div>
        <div id="textCss">
            <textarea id="cssTextarea"></textarea>
        </div>
    </div>
    
</div>
 
