<?php

namespace cubegroup\gradient;
use yii\base\Widget;
use yii\helpers\Html;






class GradientWidget extends Widget{
    
    public function init(){
        parent::init();
        $this->registerAssets();
        
    }
    
    public function run(){
        
       return $this->render('gradient.php');
        
    }
    public function registerAssets(){
        $view = $this->getView();
        GradientAsset::register($view);
    }
    
    
}