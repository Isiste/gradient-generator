/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//-------funkcje obslugujace zamiane kolorow-----------------------------------
   function fillZero(myString) {
        if (myString.length == 1) return "0" + myString;
        else return myString;
    }
     function convertFromDevToHex(color){      
        var kolor =parseInt(color) ;
        var wart = kolor.toString(16);
        return fillZero(wart);
     }
//----------------funkcja tworzaca kod css---------------------------------------
var redPoczatek="00";
var greenPoczatek="00";
var bluePoczatek="00";
var redKoniec="00";
var greenKoniec="00";
var blueKoniec="00";
var typ='linear';
var hPosPoczatek=0;
var hPosKoniec=0;
var vPosPoczatek=0;
var vPosKoniec = 0;
var radialPoczatek=0;
var radialKoniec=0;
var opacity=0;

function generujCss(){
    var kolory="";
    kolory=" from(#"+redPoczatek+greenPoczatek+bluePoczatek+"), to(#"+redKoniec+greenKoniec+blueKoniec+"))";
    if(typ=="linear"){
        var pozycje = "background:-webkit-gradient("+typ+","+hPosPoczatek+"% "+vPosPoczatek+"%, "+hPosKoniec+"% "+vPosKoniec+"%, ";    
        var zmienna = "-webkit-gradient("+typ+","+hPosPoczatek+"% "+vPosPoczatek+"%, "+hPosKoniec+"% "+vPosKoniec+"%, from(#"+redPoczatek+greenPoczatek+bluePoczatek+"), to(#"+redKoniec+greenKoniec+blueKoniec+"))";  
    }else{
        var pozycje = "background:-webkit-gradient("+typ+","+hPosPoczatek+" "+vPosPoczatek+", "+radialPoczatek+", "+hPosKoniec+" "+vPosKoniec+", "+radialKoniec+", ";
        var zmienna = "-webkit-gradient("+typ+","+hPosPoczatek+" "+vPosPoczatek+", "+radialPoczatek+", "+hPosKoniec+" "+vPosKoniec+", "+radialKoniec+", "+kolory;
    }
    var styl=pozycje+kolory;
    $("#wypelnienieGradient").css("background",zmienna);
    $("#cssTextarea").val(styl);
}


//---------kolor czerwony--------------------------------------------------------
$("#redStart").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    redPoczatek=convertFromDevToHex(wartosc);
    generujCss();

});
$("#redKoniec").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    redKoniec = convertFromDevToHex(wartosc);
    generujCss();

});
//------------------kolor zielony----------------
$("#greenStart").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    greenPoczatek = convertFromDevToHex(wartosc);
    generujCss();

});
$("#greenKoniec").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    greenKoniec = convertFromDevToHex(wartosc);
    generujCss();

});
//-----------------kolor niebieski------------------------
$("#blueStart").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    bluePoczatek = convertFromDevToHex(wartosc);
    generujCss();

});
$("#blueKoniec").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    blueKoniec = convertFromDevToHex(wartosc);
    generujCss();

});
//-------------------------pozycja h--------------------------------
$("#hPosStart").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    hPosPoczatek = wartosc;
    generujCss();
});
$("#hPosKoniec").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    hPosKoniec = wartosc;
    generujCss();
});
//-------------------------pozycja v----------------------------------
$("#vPosStart").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    vPosPoczatek = wartosc;
    generujCss();
});
$("#vPosKoniec").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    vPosKoniec = wartosc;
    generujCss();
});
//------------------------typ-----------------------------------------
$("#gradientType").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    typ = wartosc;
    if(wartosc=="linear"){
        $(".radial").hide();
    }else{
        $(".radial").show(); 
    }
    generujCss();
});
//---------------------radiale------------------------------------------
$("#radialKoniec").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    radialKoniec = wartosc;
    generujCss(); 
});
$("#radialStart").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    radialPoczatek = wartosc;
    generujCss();
});
//-------------------------opacity----------------------------------------
$("#cssOpac").on("change",function(){
    var _this = $(this);
    var wartosc = _this.val();
    opacity = wartosc;
    generujCss();
});